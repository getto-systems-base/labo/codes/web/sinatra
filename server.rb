require "sinatra"

require "version"
require "app_name/api/core"

app_name_api = GettoCodes::AppName::Api::Core.new ENV["APP_NAME_URL"]

configure do
  set :bind, "0.0.0.0"
  set :port, 3000
end

before "*" do
  content_type 'application/json'
end

get "/" do
  { message: "hello, world!!" }.to_json
end

get "/version" do
  {
    sinatra: GettoCodes::VERSION,
    app_name: app_name_api.version,
  }.to_json
end
